from setuptools import setup, find_packages

setup(
    name='controle_coherence',
    packages=find_packages(),
    version='1.5.3',
    url='https://gitlab.com/observatoire-dpe/observatoire-dpe')
